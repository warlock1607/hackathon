import { createReducer } from 'redux-act';
import {
  login,
  logout,
  addUser,
  addUsers,
  removeUser,
  newMessage,
} from '../actions/chat';

const initial = {
  app: {
    login: false,
    username: null,
    [logout]: state => ({ ...state, users: {} }),
  },
  users: {},
  messages: {
    list: [],
    entities: {},
  },
};

export const app = createReducer(
  {
    [login]: (state, payload) => ({
      ...state,
      login: true,
      username: payload.username,
    }),
    [logout]: state => ({ ...state, login: false, username: null }),
  },
  initial.app
);

export const users = createReducer(
  {
    [addUser]: (state, payload) => ({ ...state, [payload.username]: true }),
    [addUsers]: (state, payload) => {
      let newState = { ...state };
      payload.usernames.forEach(username => {
        newState = { ...newState, [username]: true };
      });
      return newState;
    },
    [removeUser]: (state, payload) => {
      const newState = { ...state };
      delete newState[payload.username];
      return newState;
    },
    [logout]: () => ({}),
  },
  initial.users
);

export const messages = createReducer(
  {
    [newMessage]: (state, payload) => {
      const { message } = payload;
      return {
        ...state,
        list: [...state.list, message.id],
        entities: { ...state.entities, [message.id]: message },
      };
    },
    [logout]: state => ({
      ...state,
      list: [],
      entities: {},
    }),
  },
  initial.messages
);
