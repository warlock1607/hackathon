import express from 'express';
import cors from 'cors';

import React from 'react';
import { StaticRouter } from 'react-router-dom';
import { renderToString } from 'react-dom/server';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import StyleProvider from '../shared/StyleProvider';

import config from '../../config';
import renderPage from './renderPage';
import App from '../shared/App';
import rootReduser from '../shared/redusers/rootReduser';

const app = express();

app.use(cors());
app.use(express.static('dist'));

app.get('*', (req, res) => {
  const context = {};
  const store = createStore(rootReduser);
  const css = new Set();
  const stylesheet = {
    insertCss: (...styles) => styles.forEach(style => css.add(style._getCss())),
  };
  const html = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <StyleProvider context={stylesheet}>
          <App />
        </StyleProvider>
      </StaticRouter>
    </Provider>
  );

  const preloadedState = store.getState();
  res.send(renderPage(html, preloadedState, css));
});

app.listen(config.port, () => {
  console.log(`app started on port ${config.port}`);
});
