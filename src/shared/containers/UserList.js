import { connect } from 'react-redux';

import UserList from '../components/Users/UserList';

const mapStateToProps = state => ({
  users: state.users,
});

const ConnectedUserList = connect(mapStateToProps)(UserList);

export default ConnectedUserList;
