import { connect } from 'react-redux';

import Welcome from '../components/Welcome/Welcome';

const mapStateToProps = state => ({
  login: state.app.login,
});

const ConnectedWelcome = connect(mapStateToProps)(Welcome);

export default ConnectedWelcome;
