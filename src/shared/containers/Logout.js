import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { logout } from '../actions/chat';

class Logout extends React.Component {
  handleLogout = () => {
    const { dispatch } = this.props;
    dispatch(logout());
  };

  render() {
    return (
      <button type="button" onClick={this.handleLogout}>
        Logout
      </button>
    );
  }
}

Logout.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Logout);
