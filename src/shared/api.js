import axios from 'axios';

import config from '../../config';

axios.defaults.baseURL = config.api;

export default function fetchRandomDog() {
  return axios.get('/breeds/image/random');
}
