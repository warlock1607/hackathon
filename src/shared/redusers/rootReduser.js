import { combineReducers } from 'redux';

import { app, users, messages } from './chat';

export default combineReducers({
  app,
  users,
  messages,
});
