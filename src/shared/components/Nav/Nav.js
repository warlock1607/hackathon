import React from 'react';
import { Link } from 'react-router-dom';

const Nav = () => (
  <section>
    <Link to="/">Home</Link>
    <Link to="/chat">Chat</Link>
  </section>
);

export default Nav;
