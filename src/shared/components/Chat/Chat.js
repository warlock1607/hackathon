import React from 'react';

import Logout from '../../containers/Logout';
import SendMessage from '../../containers/SendMessage';
import UserList from '../../containers/UserList';
import MessageList from '../../containers/MessageList';

const Chat = () => (
  <div>
    <Logout />
    <SendMessage />
    <UserList />
    <MessageList />
  </div>
);

export default Chat;
