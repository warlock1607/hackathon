import { fork } from 'redux-saga/effects';
import chatSaga from './chat';

export default function* rootSaga() {
  yield fork(chatSaga);
}
