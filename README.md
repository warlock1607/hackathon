# react-redux-saga

## Базовый шаблон React-redux-saga

### Включает в себя:
Ядро:

- React
- Redux
- Redux-saga
- Express

Сборка:
- Webpack 
- Babel

Линтинг:
- ESlint (конфигурация airbnb react + собственные правила)
- Prettier
- Pre-commit hook (lint-staged + husky)

Тестирование:
- Jest
- Enzyme


----
### Изменение в правилах линтера:
 - Разрешено писать компоненты в js файлах
 - Разрешено оставлять console.log
 - Разрешено подключение в скрипты зависимостей из devDependencies
 - Разрешены нижние подчеркивания

----
### Использование:

 -  Режим разработки:

        npm run dev

 - Сборка проекта

        npm run build

 - Запуск

        npm start

 -  Автофикс (синтаксис)

        npm run eslint

 - Автоформатирование
        
        npm run prettier
        
 - Юнит-тесты

        npm test

 - Проверка pre-commited файлов ( ручной запуск )

        npm run precommit



  




