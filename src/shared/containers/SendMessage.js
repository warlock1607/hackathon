import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { sendMessage } from '../actions/chat';

class SendMessage extends React.Component {
  handleSendMessage = event => {
    event.preventDefault();
    const text = new FormData(event.target).get('send_message');
    const { dispatch } = this.props;
    dispatch(sendMessage({ text }));
  };

  render() {
    return (
      <form onSubmit={this.handleSendMessage}>
        <input
          id="send_message"
          name="send_message"
          placeholder="send message"
          type="text"
        />
        <button type="submit">Send</button>
      </form>
    );
  }
}

SendMessage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(SendMessage);
