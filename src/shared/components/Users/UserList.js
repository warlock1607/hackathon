/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const UserList = props => {
  const { users } = props;
  return (
    <React.Fragment>
      <p>Users:</p>
      <ul>
        {Object.keys(users).map((username, i) => (
          <li key={`${i}:${username}`}>{username}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

UserList.propTypes = {
  users: PropTypes.object.isRequired,
};

export default UserList;
