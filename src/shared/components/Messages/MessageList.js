/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';

const MessageList = props => {
  const { messages } = props;
  return (
    <React.Fragment>
      <p>Messages:</p>
      <ul>
        {messages.list.map(id => messages.entities[id]).map((m, i) => (
          <li key={`${i}:${m.id}`}>{`${m.username}:${m.text}`}</li>
        ))}
      </ul>
    </React.Fragment>
  );
};

MessageList.propTypes = {
  messages: PropTypes.object.isRequired,
};

export default MessageList;
