import Home from './components/Home/Home';
import Welcome from './containers/Welcome';

const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/chat',
    exact: true,
    component: Welcome,
  },
];

export default routes;
