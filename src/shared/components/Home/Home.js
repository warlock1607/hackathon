import React from 'react';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';

const Home = () => <h1 className="home">Home</h1>;

export default withStyles(s)(Home);
