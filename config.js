export default {
  port: 8080,
  chat_api: 'http://localhost:3000',
  api: 'https://dog.ceo/api',
};
