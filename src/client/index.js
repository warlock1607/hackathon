import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { Provider } from 'react-redux';

import StyleProvider from '../shared/StyleProvider';

import rootReduser from '../shared/redusers/rootReduser';

import App from '../shared/App';
import rootSaga from '../shared/sagas/rootSaga';

const context = {
  insertCss: (...styles) => {
    const removeCss = styles.map(x => x._insertCss());
    return () => {
      removeCss.forEach(f => f());
    };
  },
};

const preloadedState = window.__PRELOADED_STATE__;

delete window.__PRELOADED_STATE__;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReduser,
  preloadedState,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

sagaMiddleware.run(rootSaga);

const app = document.getElementById('app');

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <StyleProvider context={context}>
        <App />
      </StyleProvider>
    </BrowserRouter>
  </Provider>,
  app,
  () => {
    const ssStyles = document.getElementById('server-side-styles');
    ssStyles.parentNode.removeChild(ssStyles);
  }
);
