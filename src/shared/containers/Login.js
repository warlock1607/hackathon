import React from 'react';
import PropTypes from 'prop-types';

import { connect } from 'react-redux';
import { login } from '../actions/chat';

class Login extends React.Component {
  handleLogin = event => {
    event.preventDefault();
    const username = new FormData(event.target).get('login');
    const { dispatch } = this.props;
    dispatch(login({ username }));
  };

  render() {
    return (
      <form onSubmit={this.handleLogin}>
        <input id="login" name="login" placeholder="login" type="text" />
        <button type="submit">Login</button>
      </form>
    );
  }
}

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

export default connect()(Login);
