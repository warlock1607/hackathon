import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Welcome.css';

import Login from '../../containers/Login';
import Chat from '../Chat/Chat';

const Welcome = props => {
  const { login } = props;
  return (
    <div>
      <h1 className="welcome">Chat</h1>
      {login ? <Chat /> : <Login />}
    </div>
  );
};

Welcome.propTypes = {
  login: PropTypes.bool.isRequired,
};

export default withStyles(s)(Welcome);
