import { connect } from 'react-redux';

import MessageList from '../components/Messages/MessageList';

const mapStateToProps = state => ({
  messages: state.messages,
});

const ConnectedMessageList = connect(mapStateToProps)(MessageList);

export default ConnectedMessageList;
